package com.example.walpaperapp;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public
class MainActivity extends AppCompatActivity {


    int on = 0;

    private
    android.widget.ImageView imageView;


    private androidx.recyclerview.widget.RecyclerView recyclerView;
    private com.example.walpaperapp.CustomAdapter     acustomAdapter;
    private
    android.widget.LinearLayout linearLayout;
    private
    android.widget.TextView textView;
    int imagepostion;


    @Override
    protected
    void onCreate ( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_main );

        recyclerView = findViewById ( com.example.walpaperapp.R.id.RecylerViewId );//finding
        imageView    = findViewById ( com.example.walpaperapp.R.id.ImageMain );//finding

        linearLayout=findViewById ( com.example.walpaperapp.R.id.setLinearId );//finding
        textView=findViewById ( com.example.walpaperapp.R.id.TextSet );//finding

        android.content.Intent intent = getIntent ( );

        imagepostion= intent.getIntExtra ( "image" , 00 );
        imageView.setImageResource ( imagepostion );


        int[] image = new int[] {
                com.example.walpaperapp.R.drawable.image1 , com.example.walpaperapp.R.drawable.image2 , com.example.walpaperapp.R.drawable.image3 , com.example.walpaperapp.R.drawable.images4 , com.example.walpaperapp.R.drawable.images5 , com.example.walpaperapp.R.drawable.images6 , com.example.walpaperapp.R.drawable.images7 , com.example.walpaperapp.R.drawable.images8 , com.example.walpaperapp.R.drawable.images9 , com.example.walpaperapp.R.drawable.images10 , com.example.walpaperapp.R.drawable.images11 , com.example.walpaperapp.R.drawable.images12 , com.example.walpaperapp.R.drawable.images13 , com.example.walpaperapp.R.drawable.images14 , com.example.walpaperapp.R.drawable.images15 , com.example.walpaperapp.R.drawable.images16 , com.example.walpaperapp.R.drawable.images17 , com.example.walpaperapp.R.drawable.images18 , com.example.walpaperapp.R.drawable.images19


        };

        recyclerView.setLayoutManager ( new androidx.recyclerview.widget.LinearLayoutManager ( this , androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL , false ) );

        acustomAdapter = new CustomAdapter ( this , image );
        recyclerView.setAdapter ( acustomAdapter );

        linearLayout.setOnClickListener ( new android.view.View.OnClickListener ( ) {
            @Override
            public
            void onClick ( android.view.View v ) {

                setwallpaper();

            }
        } );


    }//secondBacket

    public
    void Showset ( android.view.View view ) {


        if ( on == 0 ) {
            on++;
            textView.setVisibility ( android.view.View.VISIBLE );

            linearLayout.setVisibility ( android.view.View.VISIBLE );


        }
        else if(on!=0){
            --on;
            textView.setVisibility ( android.view.View.INVISIBLE );
            linearLayout.setVisibility ( android.view.View.INVISIBLE );

        }



    }
    public void setwallpaper(){
        android.app.WallpaperManager wallpaperManager= android.app.WallpaperManager.getInstance ( MainActivity.this );

        if ( imagepostion != 0 ) {

            try {
                android.widget.Toast.makeText ( MainActivity.this, "Wallpaper Set", android.widget.Toast.LENGTH_SHORT ).show ();
                wallpaperManager.setResource (imagepostion);
            }
            catch ( java.io.IOException e ) {
                e.printStackTrace ( );
            }

        }
       else if(imagepostion==0){


           int tempimage= com.example.walpaperapp.R.raw.image2;

            try {
                android.widget.Toast.makeText ( MainActivity.this, "Wallpaper Set", android.widget.Toast.LENGTH_SHORT ).show ();
                wallpaperManager.setResource (tempimage);
            }
            catch ( java.io.IOException e ) {
                e.printStackTrace ( );
            }

        }



    }
}//last breket