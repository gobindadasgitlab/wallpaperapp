package com.example.walpaperapp;

import android.widget.ListView;

public
class CustomAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter< com.example.walpaperapp.CustomAdapter.CustomHolder > {
android.content.Context context;
int[]images;

    public
    CustomAdapter ( android.content.Context context , int[] images ) {
        this.context = context;
        this.images  = images;
    }

    @Override
    public
    com.example.walpaperapp.CustomAdapter.CustomHolder onCreateViewHolder ( @androidx.annotation.NonNull android.view.ViewGroup parent , int viewType ) {


        android.view.LayoutInflater layoutInflater= android.view.LayoutInflater.from (context );
        android.view.View   view=layoutInflater.inflate ( com.example.walpaperapp.R.layout.image_list,parent,false );

        return new com.example.walpaperapp.CustomAdapter.CustomHolder ( view );
    }


    @Override
    public
    void onBindViewHolder ( @androidx.annotation.NonNull com.example.walpaperapp.CustomAdapter.CustomHolder holder , int position )
    {

        final int image =images[position];

        holder.imageView.setImageResource (image);
        holder.linearLayout.setOnClickListener ( new android.view.View.OnClickListener ( ) {
            @Override
            public
            void onClick ( android.view.View v ) {
                android.content.Intent intent =new android.content.Intent (  context,MainActivity.class);
                intent.putExtra ( "image",image);
               context.startActivity ( intent );



            }
        } );


    }

    public
    int getItemCount ( ) {

        return images.length;
    }

    public  class  CustomHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {//CustomHolderClass


        private
        android.widget.ImageView imageView;
        private
        android.widget.LinearLayout linearLayout;



    public
    CustomHolder ( @androidx.annotation.NonNull android.view.View itemView ) {//Conustructor


        super ( itemView );


        imageView=itemView.findViewById ( com.example.walpaperapp.R.id.ImageView );
        linearLayout=itemView.findViewById ( com.example.walpaperapp.R.id.LinearId );


    }



}

}
// lastBraket
